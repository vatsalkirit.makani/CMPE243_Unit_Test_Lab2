/*
 * test_queue_hw.c
 *
 *  Created on: 04-Mar-2019
 *      Author: Vatsalm93
 */
#include "unity.h"
#include <stdint.h>
#include "queue_hw.h"


void test_queue_init(void)
{
    queue_S queue={};
    queue_init(&queue);
    TEST_ASSERT_EQUAL(0,queue.front);
    TEST_ASSERT_EQUAL(99,queue.rear);
    TEST_ASSERT_EQUAL(0,queue.size);
}

void test_queue_push(void)
{
    queue_S queue={};

    //Passing Test
    for(int i=0;i<100;i++)
    {
    TEST_ASSERT_TRUE(queue_push(&queue,i));
    }

//    //Intentional Failure: Pushing greater than size of queue
//    for(int i=0;i<120;i++)
//    TEST_ASSERT_TRUE(queue_push(&queue,i));
}

void test_queue_pop(void)
{
    queue_S queue={};
    uint8_t pop_val[100]={0};

    //Pushing into queue before popping
    for(int i=0;i<100;i++)
     {
     TEST_ASSERT_TRUE(queue_push(&queue,i));
     }

    //Pop 10 elements and store into array pop_val[]
    for(int i=0;i<10;i++)
    {
    TEST_ASSERT_TRUE(queue_pop(&queue,&pop_val[i]));
    }
}

void test_queue_get_count(void)
{
    queue_S queue={};
    uint8_t pop_val[100]={0};

    //Pushing into queue and checking queue size
    for(int i=0;i<100;i++)
     {
     TEST_ASSERT_TRUE(queue_push(&queue,i));
     }
     TEST_ASSERT_EQUAL(100,queue_get_count(&queue));

    //Popping 25 elements from queue and checking queue size
    for(int i=0;i<25;i++)
      {
      TEST_ASSERT_TRUE(queue_pop(&queue,&pop_val[i]));
      }
      TEST_ASSERT_EQUAL(75,queue_get_count(&queue));
}
