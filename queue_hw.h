/*
 * queue_hw.h
 *
 *  Created on: 04-Mar-2019
 *      Author: Vatsalm93
 */

#ifndef QUEUE_HW_H_
#define QUEUE_HW_H_

#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"
#include "stddef.h"

typedef struct {
  uint8_t queue_memory[100];
  uint8_t front;
  uint8_t rear;
  size_t size;
} queue_S;

void queue_init(queue_S *queue);

bool queue_push(queue_S *queue, uint8_t push_value);

bool queue_pop(queue_S *queue, uint8_t *pop_value);

size_t queue_get_count(queue_S *queue);



#endif /* QUEUE_HW_H_ */
