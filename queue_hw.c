/*
 * queue_hw.c
 *
 *  Created on: 04-Mar-2019
 *      Author: Vatsalm93
 */
#include "queue_hw.h"
#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"
#include "stddef.h"
#define maxsize 100

void queue_init(queue_S *queue)
{
    queue->front=0;
    queue->rear=maxsize-1;
    queue->size=0;
}

bool queue_push(queue_S *queue, uint8_t push_value)
{
    if(queue->size==maxsize)
    {
        printf("Queue is full\n");
        return false;
    }
    else
    {
        queue->rear=(queue->rear+1)% maxsize;
        queue->size++;
        queue->queue_memory[queue->rear]= push_value;
    }
    return true;
}

bool queue_pop(queue_S *queue, uint8_t *pop_value)
{
    if(queue->size==0)
        {
            printf("Queue is empty\n");
            return false;
        }
        else
        {
            *pop_value= queue->queue_memory[queue->front];
            queue->size--;
            queue->front=(queue->front+1)%maxsize;
        }
    return true;
}

size_t queue_get_count(queue_S *queue)
{
return queue->size;
}

